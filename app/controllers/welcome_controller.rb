require 'twitter'

class WelcomeController < ActionController::Base
	def index
		@welcome = 'Locaweb'
	end

	def tweet
		uri = URI.parse("http://xxxcnn0330.locaweb.com.br/tweeps")

		@url = uri

		http = Net::HTTP.new(uri.host, uri.port)
		request = Net::HTTP::Get.new(uri.request_uri)
		request['Username'] = "linux.soares@gmail.com"
		response = http.request(request)

		json_hash = JSON.parse(response.body)

		@tweets = json_hash["statuses"].map { |h| h['entities']}.select {|h| h['user_mentions'].one? { |m| m['id'] == 42 } }
	end
end
